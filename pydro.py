# -*- coding: utf-8 -*-
from abc import abstractmethod, abstractproperty, ABCMeta
from networkx import topological_sort

__all__ = ['StrahlerCalculator', 'ShreveCalculator']


class MetricNode(object):

    """ Classe abstrata que representa um nó na DP do cálculo de uma métrica.
        Cada nova métrica deve implementar um nó conforme o algoritmo para
        cálculo da mesma.
    """

    __metaclass__ = ABCMeta

    def __init__(self):
        self.value = 0

    @abstractmethod
    def compute_value(self):

        """ Computa o valor do nó.
            O método só deve ser chamado quando o nó já possui toda a
            informação necessária vinda de seus antecessores.
        """

        pass

    @abstractmethod
    def update_data(self, val):

        """ Atualiza os dados do nó conforme a informação (val) enviada
            por um de seus antecessores
        """

        pass


class MetricCalculator(object):

    """ Classe abstrata para cálculo de uma métrica.
        As classes dervidas podem apenas definir qual o MetricNode
        utilizado no algoritmo.
    """

    __metaclass__ = ABCMeta

    @abstractproperty
    def metric_node(self):

        """ MetricNode a ser utilizado no cálculo """

        pass

    def __init__(self, digraph):

        """ Recebe um networkx.DiGraph já preenchido com 
            os dados dos vértices e arestas. 
        """

        self.digraph = digraph
        self.metric_data = None  # Dicionário que será utilizado na DP
        self.__calculated = False  # Indica se a métrica já foi calculada

    def calculate(self):

        """ Calcula a métrica necessária """

        sorted_nodes = topological_sort(self.digraph)

        # Inicializa a tabela da DP
        self.metric_data = dict((x, self.metric_node()) for x in sorted_nodes)

        for node in sorted_nodes:

            # Computa o valor para o vértice, o qual já recebeu todas
            # as informaçẽos necessárias
            self.metric_data[node].compute_value()

            for s in self.digraph.successors(node):

                # Envia a informação calculada para cada sucessor do nó.
                self.metric_data[s].update_data(self.metric_data[node].value)

        self.__calculated = True

    def node_value(self, node):

        """ Retorna a métrica obtida para o vértice
            com identificador 'node'
        """

        if not self.__calculated:
            self.calculate()

        return self.metric_data[node].value


class StrahlerNode(MetricNode):

    """ Nó da DP para o cálculo da ordem Strahler """

    def __init__(self):
        super(StrahlerNode, self).__init__()

        # Máximo já visto pelo nó (em relação aos valores dos antecessores)
        self.max_so_far = None

        # Indica se o máximo já visto foi visto mais de uma vez
        self.duplicate = False

    def compute_value(self):

        if self.max_so_far is None:
            # Se o máximo ainda era None, é porque é uma nascente
            self.value = 1
        else:
            # Se o máximo já visto não é duplicado, o valor é ele.
            # Caso haja mais de um é ele+1.
            self.value = self.duplicate and self.max_so_far+1 or self.max_so_far

    def update_data(self, val):

        if self.max_so_far is None or val > self.max_so_far:

            # Se o valor do antecessor é maior que o máximo já visto,
            # coloca ele como novo valor não duplicado.
            self.max_so_far = val
            self.duplicate = False

        elif self.max_so_far == val:

            # Se o valor é igual ao máximo já visto, marca como duplicado
            self.duplicate = True


class StrahlerCalculator(MetricCalculator):

    """ Calculadora de ordem Strahler. """

    metric_node = StrahlerNode


class ShreveNode(MetricNode):

    """ Nó da DP para o cálculo da ordem Shreve """

    def compute_value(self):

        # Se o valor ainda era zero, é porque é uma nascente

        if self.value == 0:
            self.value = 1

    def update_data(self, val):
        self.value += val


class ShreveCalculator(MetricCalculator):

    """ Calculadora da ordem Shreve. """

    metric_node = ShreveNode


if __name__ == '__main__':

    # Testa se os algoritmos estão funcionando corretamente

    from networkx import DiGraph

    d = DiGraph()
    d.add_edge(1, 3)
    d.add_edge(2, 3)
    d.add_edge(3, 5)
    d.add_edge(4, 5)
    d.add_edge(5, 9)
    d.add_edge(6, 8)
    d.add_edge(7, 8)
    d.add_edge(8, 9)
    d.add_edge(9, 10)
    d.add_edge(10, 11)
    d.add_edge(12, 11)
    d.add_edge(13, 15)
    d.add_edge(14, 15)
    d.add_edge(15, 16)
    d.add_edge(17, 18)
    d.add_edge(19, 18)
    d.add_edge(18, 16)
    d.add_edge(16, 9)
    d.add_edge(16, 20)

    strahler = StrahlerCalculator(d)
    strahler.calculate()
    assert(strahler.node_value(11) == 3)
    assert(strahler.node_value(20) == 3)
    print "Strahler OK"

    shreve = ShreveCalculator(d)
    shreve.calculate()
    assert(shreve.node_value(11) == 10)
    assert(shreve.node_value(20) == 4)
    print "Shreve OK"
