#!/usr/bin/env python
# -*- coding: utf-8 -*-
from networkx import DiGraph
from argparse import ArgumentParser, ArgumentDefaultsHelpFormatter

from connection import Connection
from pydro import StrahlerCalculator, ShreveCalculator


# Para adicionar uma métrica nova, basta adicionar
# uma tupla ('nome', ClasseDaCalculadora) na lista abaixo:

METRICAS = [('strahler', StrahlerCalculator),
            ('shreve',   ShreveCalculator)]


class Edge(object):

    """ Representa uma aresta lida do banco de dados.
        Classe utilizada apenas para melhorar a leitura.
    """

    def __init__(self, t):

        """ Recebe uma tupla no formato (id, from_node, to_node) """

        self.id = t[0]
        self.from_node = t[1]
        self.to_node = t[2]


def _setup_args():

    parser = ArgumentParser(formatter_class=ArgumentDefaultsHelpFormatter)

    parser.add_argument('-m', '--metrica',
                        action='append', dest='metricas',
                        choices=[x[0] for x in METRICAS], default=[],
                        help=u'Métrica a ser calculada')

    parser.add_argument('-hs', '--host', default='localhost',
                        help=u'Host para a conexão Postgresql')

    parser.add_argument('-u', '--user', default='postgres',
                        help=u'Usuário da conexão Postgresql')

    parser.add_argument('-p', '--password', default='',
                        help=u'Senha para a conexão Postgresql')

    parser.add_argument('-fn', '--from_node', default='fn',
                        help=u'Campo na tabela '
                        u'que representa o nó de origem da aresta')

    parser.add_argument('-tn', '--to_node', default='tn',
                        help=u'Campo na tabela '
                        u'que representa o nó de destino da aresta')

    parser.add_argument('-id', '--edge_id', default='id',
                        help=u'Campo na tabela '
                        u'que representa a primary key da aresta')

    for m in METRICAS:
        parser.add_argument('--ordem_{0}'.format(m[0]),
                            default='ordem_{0}'.format(m[0]),
                            help=u'Campo na tabela onde a ordem {0} será salva'
                            u' (caso seja calculada).'.format(m[0]))

    parser.add_argument('database',
                        help=u'DB Postgresql onde estão armazenadas '
                        u'as arestas e onde as operações serão executadas')

    parser.add_argument('table',
                        help=u'Tabela onde estão armazenadas '
                        u'as arestas e onde as operações serão executadas')

    return parser.parse_args()


if __name__ == '__main__':

    args = _setup_args()

    if not args.metricas:
        print u'Nenhuma métrica informada. (use -m métrica).'
        exit()

    pg_params = "host='{0}' dbname='{1}' "\
                "user='{2}' password='{3}'".format(args.host,
                                                   args.database,
                                                   args.user,
                                                   args.password)
    conn = Connection(pg_params)

    print "Lendo arestas..."

    query = "SELECT {0}, {1}, {2} FROM {3}".format(args.edge_id,
                                                   args.from_node,
                                                   args.to_node,
                                                   args.table)
    edges = [Edge(t) for t in conn.query(query)]

    print u"Criando grafo..."

    digraph = DiGraph()

    for e in edges:
        digraph.add_edge(e.from_node, e.to_node)

    cur = conn.cursor()

    for m in [t for t in METRICAS if t[0] in args.metricas]:
        print "Calculando ordens {0}...".format(m[0].title())

        field_attr = getattr(args, 'ordem_{0}'.format(m[0]))

        calc = m[1](digraph)
        calc.calculate()

        print u"Salvando ordens {0} no banco de dados...".format(m[0].title())

        for e in edges:

            edge_value = calc.node_value(e.from_node)

            query = "UPDATE {0} SET {1} = %s WHERE id = %s".format(args.table,
                                                                   field_attr)

            cur.execute(query, (edge_value, e.id))

    conn.commit()
