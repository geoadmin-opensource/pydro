# -*- coding: utf-8 -*-
import psycopg2
import psycopg2.extensions

psycopg2.extensions.register_type(psycopg2.extensions.UNICODE)
psycopg2.extensions.register_type(psycopg2.extensions.UNICODEARRAY)

class Connection(object):

	""" Representa uma conexão com um banco de dados PostgreSQL """

	def __init__(self, pg_params):

		""" Recebe uma string de parâmetros PostgreSQL """

		self.__connection = psycopg2.connect(pg_params)


	def cursor(self):

		""" Retorna um cursor da conexão """

		return self.__connection.cursor()
	
	def query(self, query, qargs=None):

		""" Executa uma query, retornando uma lista com os dados obtidos. """

		c = self.cursor()
		c.execute(query, qargs)
		data = c.fetchall()
		c.close()
		
		return data


	def commit(self):
		
		self.__connection.commit()